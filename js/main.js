let counter;

$(document).ready(function () {
    let data = posaciJson[0],
        areaList = "";

    for (let i = 0; i < data.ponudjene.length; i++) {
        areaList += '<option value="' + data.ponudjene[i] + '"/>'
    }
    $('#area-list').html(areaList);
    $('#region-info').html(data.oblast);

    $('#add-area').on('click', function () {
        let areaInputElement = $('#area-input'),
            html = "";
        html += '<div class="area-item">';
        html += '<span class="area-name">' + areaInputElement.val() + '</span>';
        html += '<i class="remove-item glyphicon glyphicon-trash pull-right"></i>';
        html += '</div>';

        $('#pick-container').append(html);
        areaInputElement.val('');
    });

    $('#pick-container').on('click', '.remove-item', function () {
        $(this).parent().remove();
    });

    $('#get-results').on('click', function () {
        let correctAnswersList = data.tacno,
            totalAnswers = 0,
            correct = 0,
            timeUsed = secondsToHuman(seconds);

        $('.area-name').each(function () {
            totalAnswers++;
            for (let i = 0; i < correctAnswersList.length; i++)
                if ($(this).html().indexOf(correctAnswersList[i]) !== -1)
                    correct++;
        });

        finishGame();

        $('#result-correct-answers').html(correct);
        $('#result-time').html(timeUsed);

        $('#results-modal').modal("show");

        initResultsBar(correct !== 0 ? correct / totalAnswers * 100 : 0);
    });

    let seconds = data.vreme;
    $("#time-left").html(secondsToHuman(seconds));

    counter = setInterval(function () {
        if (seconds > 0) {
            $("#time-left").html(secondsToHuman(--seconds));
        } else {
            finishGame();
        }
    }, 1000);
});

function finishGame() {
    $('#time-up-notice').show();
    $('#area-input').prop("disabled", true);
    $('#add-area').prop("disabled", true);
    $('.remove-item').each(function () {
        this.remove();
    });

    clearInterval(counter);
}

function initResultsBar(percent) {
    let elem = $('#progress-bar');
    let width = 0;
    let run = setInterval(frame, 10);

    function frame() {
        elem.css("width", width + '%');
        elem.html(width + '%');

        width >= percent ? clearInterval(run) : width++;
    }
}

/*Helper functions*/
function zeroPad(val) {
    return val > 9 ? val : "0" + val;
}

function secondsToHuman(seconds) {
    let min = Math.floor((((seconds % 31536000) % 86400) % 3600) / 60);
    let sec = (((seconds % 31536000) % 86400) % 3600) % 60;
    return min !== 0 ? min + "<span class='wb-ghost-text'>m</span>" + zeroPad(sec) + "<span class='wb-ghost-text'>s</span>" : "" + zeroPad(sec) + "<span class='wb-ghost-text'>s</span>";
}
